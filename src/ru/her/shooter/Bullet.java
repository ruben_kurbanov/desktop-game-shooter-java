package ru.her.shooter;

import java.awt.*;

public class Bullet {

   //Fields
   private double x;
   private double y;
   
   private int r;
   private int speed;
   
   private double bulletDX;
   private double bulletDY;

   private Color color;
   
   private GamePanel gamePanel;
   
   
   //constructor
   
   public Bullet() {
      
      x = GamePanel.player.getX();
      y = GamePanel.player.getY() - 50;
      
      r = 3;
      
      speed = -10;

      bulletDX = 0;
      bulletDY = speed;
      
      color = Color.WHITE;
      
      gamePanel = new GamePanel();
   }
   
   //Functions
   
   public double getX() {return x;}
   
   public double getY() {return y;}
   
   public int getR() {return r;}
   
   public boolean remove() {
      
      if(y < 0 || y > gamePanel.HEIGHT || x < 0 || x > gamePanel.WIDTH) {
         return true;
      }
      return false;
   }
   
   public void update() {
      
      y += bulletDY;
      x += bulletDX;   
   }
   
   public void draw(Graphics2D g) {
      
      g.setColor(color);
      g.fillOval((int) x - r, (int) y - r, r, 15*r);

   }
   
}

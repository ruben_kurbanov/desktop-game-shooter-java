package ru.her.shooter;

import java.awt.*;

import javax.swing.ImageIcon;

public class Enemy {
   
   //Fields
 
   private long time;
   private long timer;
   private long controlTimer;
   
   private GamePanel gamePanel;
   
   Image img;

   private int r;
   private String image;
   private double speed;
   private double health;
   private double x;
   private double y;
   private double dx;
   private double dy; 
   private int type;

   //Constructor
   
   public Enemy(int type) {

      this.type = type;
      
      gamePanel = new GamePanel();
 
      time = System.nanoTime();

      switch(type) {
         case(1):
            
               r = 30;
                  
               image = "enemy.jpg";
                  
               speed = 4;
                  
               health = 2;
                  
               double angle = Math.toRadians(Math.random() * 360);
               dx = Math.sin(angle) * speed;
               dy = Math.cos(angle) * speed;
               
            break;
            
         case(2):
      
               r = 45;
               
               image = "enemy2.jpg"; 
               
               speed = 6;

               health = 5;
               
               double angle2 = Math.toRadians(Math.random() * 360);
               dx = Math.sin(angle2) * speed;
               dy = Math.cos(angle2) * speed;
               
            break;
            
         case(3):

               r = 90;
          
               image = "boss.jpg"; 
         
               speed = 2;
         
               health = 40;
         
               double angle3 = Math.toRadians(Math.random() * 360);
               dx = Math.sin(angle3) * speed;
               dy = Math.cos(angle3) * speed;
          
            break;
            
         default:
            
            break;
               
         }
      
      x = gamePanel.WIDTH * Math.random();
      y = r/2;
      
   }
   
   //Functions
   
   public double getX() {return x;}
   
   public double getY() {return y;}
   
   public int getR() {return r;}
   
   public boolean remove() {
      if(health <= 0) {
         return true;
      }
      return false;
   }
   
   public void hit() {
      health--;
   }
   
   public void update() {
  
      x += dx;
      y += dy;
      
      //border control
      if(x < r && dx < 0) dx = -dx;
      if(x > gamePanel.WIDTH - r && dx > 0) dx = -dx;
      if(y < r && dy < 0) dy = -dy;
      if(y > gamePanel.HEIGHT / 2 - r && dy > 0) dy = -dy;
      
      //bullet generation frequency
      timer = (System.nanoTime() - time) / 100000000;
      
      if(timer%20==0 && controlTimer != timer && type == 1) {
         controlTimer = timer;
         GamePanel.bulletsEnemy.add(new EnemyBullet(x, y));
      }
      if(timer%10==0 && controlTimer != timer && type == 2) {
         controlTimer = timer;
         GamePanel.bulletsEnemy.add(new EnemyBullet(x, y));
      }
      if(((int)(timer/50))%2 == 0 && type == 3) {
         controlTimer = timer;
         GamePanel.bulletsEnemy.add(new EnemyBullet(x, y));
      }
      
    
   }
   
   public void  draw(Graphics2D g) {

      img = new ImageIcon("pictures\\" + image).getImage();
      g.drawImage(img, (int) (x - r), (int) (y - r), 2*r, 2*r, null);
      
   }

}

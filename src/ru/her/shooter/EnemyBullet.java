package ru.her.shooter;

import java.awt.*;

public class EnemyBullet {

   //Fields
   
   private double x;
   private double y;
   
   private double bulletDX;
   private double bulletDY;

   private int r;
   
   private int speed;
   
   private Color color;
   
   private GamePanel gamePanel;
   
   
   //constructor
   
   public EnemyBullet(double x, double y) {
      
      this.x = x;
      this.y = y;
      
      r = 3;
      
      speed = 15;

      bulletDX = 0;
      bulletDY = speed;
      
      color = Color.WHITE;
      
      gamePanel = new GamePanel();
   }
   
   //Functions

   public double getX() {return x;}
   
   public double getY() {return y;}
   
   public int getR() {return r;}
   
   public boolean remove() {
      
      if(y < 0 || y > gamePanel.HEIGHT || x < 0 || x > gamePanel.WIDTH) {
         return true;
      }
      return false;
   }
   
   public void update() {
      y += bulletDY;
      x += bulletDX;
          
   }
   
   public void draw(Graphics2D g) {
      
      g.setColor(color);
      g.fillOval((int) x - r, (int) y - r, r, 10*r);

   }
   
}

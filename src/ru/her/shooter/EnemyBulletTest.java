package ru.her.shooter;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class EnemyBulletTest {

   EnemyBullet enemyBullet = new EnemyBullet(12.1 , 14.3);
   
   @Test
   void testPositionX() {
      double x = enemyBullet.getX();
      
      assertEquals(12.1, x);
   }
   
   @Test
   void testPositionY() {
      double y = enemyBullet.getY();
      
      assertEquals(14.3, y);
   }
   
   @Test
   void testR() {
      double r = enemyBullet.getR();
      
      assertEquals(3, r);
   }
   
   @Test
   void testRemove() {
      
      enemyBullet = new EnemyBullet(-1 , 14.3);
      boolean remove = enemyBullet.remove();
      
      assertEquals(true, remove);
   }

}

package ru.her.shooter;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class EnemyTest {
   
   Enemy enemy =  new Enemy(1);

   @Test
   void testPositionY() {
      
      double y = enemy.getY();
      
      assertEquals(enemy.getR()/2, y);
   }
   
   @Test
   void testR() {
      enemy = new Enemy(2);
      
      int r = enemy.getR();
      
      assertEquals(45, r);
   }
   
   @Test
   void testHealthEnemy() {
      
      enemy = new Enemy(1);
      enemy.hit();
      enemy.hit();
      
      boolean die = enemy.remove();
      
      assertEquals(true, die);
   }
   

}

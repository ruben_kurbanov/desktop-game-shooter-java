package ru.her.shooter;

import java.awt.*; 

import javax.swing.ImageIcon;

public class GameBack{
   
   //Fields

   Image img;
   private String image;
   
   private GamePanel gamePanel;
   
   //Constructor
   public GameBack(String s) {
      
      this.image = s;
      
      gamePanel = new GamePanel();

   }
   
   //Functions
   
   public void update() {
      
   }
   
   public void draw(Graphics2D g) {

      img = new ImageIcon("pictures\\" + image).getImage();
      g.drawImage(img, 0, 0, gamePanel.WIDTH, gamePanel.HEIGHT, null);
      
   }
   
}

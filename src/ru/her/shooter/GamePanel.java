package ru.her.shooter;

import java.awt.image.BufferedImage; 

import java.awt.*;

import javax.swing.JPanel;

import java.util.*;

import java.util.logging.Level;

import java.util.logging.Logger;

public class GamePanel extends JPanel implements Runnable{
   
   //Fields
   
   public final int WIDTH = 600;
   public final int HEIGHT = 640;
   
   private String imageMenue = "menue.jpg";
   private String imagePlay = "play.jpg";
   private String imageInstruction = "instr.jpg";
   private String imagePause = "pause.jpg";
   private String imageGameOver = "gameover.jpg";
   private String imageWin = "win.jpg";
   
   public static int mouseX;
   public static int mouseY;
   public static boolean mouseClick;
   public static boolean clickEsc;
   
   private Thread thread;
   
   private BufferedImage image; 
   private Graphics2D g; 
   
   private int FPS;
   private double millisToFPS;
   private long timerFPS;
   private int sleepTime;
   
   public static enum STATES{
      MENUE,
      PLAY,
      PAUSE,
      OVER,
      INSTR,
      VICTORY
   } 
   
   public static STATES state;
   
   public static Player player;
   public static EnemyBullet enemyBullet;
   
   private GameBack background;
   private Wave wave;
   private Menue menue;
   private Pause pause;
   private Life life;
   
   private ArrayList<Bullet> bullets;
   public static ArrayList<Enemy> enemies;
   public static ArrayList<EnemyBullet> bulletsEnemy;
   public static ArrayList<Life> countLife;
 
   //Constructor
   
   public GamePanel() {
      super();
      
      setPreferredSize(new Dimension(WIDTH, HEIGHT));
      setFocusable(true);
      requestFocus();
      
      addKeyListener(new Listeners());
      addMouseMotionListener(new Listeners());
      addMouseListener(new Listeners());
   }

   public void start() {
      thread = new Thread(this);
      thread.start();
   }
  
   public void run() {
      
      /**
       * initialization of classes and variables (for a new game)
       */
       
       //logger
       logger = Logger.getLogger(GamePanel.class.getName());
       logger.log(Level.INFO, "Game start!");
       
       while(true) {

         state = STATES.MENUE;
         FPS = 30;
         millisToFPS = 1000/FPS;
         sleepTime = 0;
     
         image = new BufferedImage(WIDTH, HEIGHT, BufferedImage.TYPE_INT_RGB);
         g = (Graphics2D) image.getGraphics();
         g.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
         
         mouseClick = false;
         clickEsc = false;
         player = new Player();
         bullets = new ArrayList<Bullet>();
         enemies = new ArrayList<Enemy>();
         bulletsEnemy = new ArrayList<EnemyBullet>();
         countLife = new ArrayList<Life>();
         wave = new Wave();
         menue = new Menue();
         pause = new Pause();
         life = new Life();
   
         Toolkit kit = Toolkit.getDefaultToolkit();
         BufferedImage buffered = new BufferedImage(16, 16, BufferedImage.TYPE_INT_ARGB);
         Graphics2D g3 = (Graphics2D) buffered.getGraphics();
         g3.setColor(new Color(255, 255, 255));
         g3.drawOval(0, 0, 4, 4);
         g3.drawLine(2, 0, 2, 4);
         g3.drawLine(0, 2, 4, 2);
         Cursor myCursor = kit.createCustomCursor(buffered, new Point(3, 3), "myCursore");
         g3.dispose();
         
         
         while(true) {
            
            this.setCursor(myCursor);
            
            timerFPS = System.nanoTime();
   
            gameDraw();
            
            /**
             * to distribute different game modes (pause, menu)
             */
            if(wave.showWaveCount() == 11) {
               state = GamePanel.STATES.VICTORY;
            }
            
            if(clickEsc) break;
   
            if(player.getHealth()<= 0) {
               state = GamePanel.STATES.OVER;
            }
            
            if(state.equals(STATES.MENUE)) {
               background = new GameBack(imageMenue);
               background.draw(g);
               menue.update();
               menue.draw(g);
            
            }
            
            if(state.equals(STATES.OVER)) {
               background = new GameBack(imageGameOver);
               background.draw(g);
            }  
            
            if(state.equals(STATES.INSTR)) {
               background = new GameBack(imageInstruction);
               background.draw(g);
            }
            
            if(state.equals(STATES.PLAY)) {
               
               background = new GameBack(imagePlay);
               gameUpdate();
               gameRender();
   
            }
            
            if(state.equals(STATES.PAUSE)) {
               background = new GameBack(imagePause);
               background.draw(g);
               pause.update();
               pause.draw(g);
            }
            
            if(state.equals(STATES.VICTORY)) {
               background = new GameBack(imageWin);
               background.draw(g);
            }
            
            timerFPS = (System.nanoTime() - timerFPS)/1000000;
            
            if(millisToFPS > timerFPS) {
               sleepTime = (int)(millisToFPS - timerFPS);
            }else sleepTime = 1;
            
            try {   
               thread.sleep(sleepTime);
               
            } catch(InterruptedException e){    
               e.printStackTrace();
              }
            
            timerFPS = 0;
            sleepTime = 1;
            
         } 
      }
   }
   
   /**
    * updating all values of game elements (movement, appearance, deletion)
    */
   public void gameUpdate() {
      
      // background update
      background.update();
      
      //player update
      player.update();
      
      //checking the conditions for creating player bullets
      if(player.generationBullet()) bullets.add(new Bullet());
      
      //bullets update
      for(int i = 0; i < bullets.size(); i++) {
         bullets.get(i).update();
         boolean remove = bullets.get(i).remove();
         if(remove) {
            bullets.remove(i);
            i--;
         }
      }

      
      //enemies update
      for(int i = 0; i < enemies.size(); i++) {
         enemies.get(i).update();
         
      }
      
      //Bullets-enemies update
      for(int i = 0; i < bulletsEnemy.size(); i++) {
     
         bulletsEnemy.get(i).update();
         boolean remove = bulletsEnemy.get(i).remove();
         if(remove) {
            bulletsEnemy.remove(i);
            i--;
         }

         for(int j = 0; j < enemies.size(); j++) {
            
            int r = (int)(Math.random()*enemies.size());
            
            Enemy e = enemies.get(r);
            double ex = e.getX();
            double ey = e.getY();
          
            enemyBullet = new EnemyBullet(ex, ey);
            
         }
               
      }
    
      //Bullets-enemies collide
      for(int i = 0; i < enemies.size(); i++) {
         Enemy e = enemies.get(i);
         double ex = e.getX();
         double ey = e.getY();
         
         for(int j = 0; j < bullets.size(); j++) {
            Bullet b = bullets.get(j);
            double bx = b.getX();
            double by = b.getY();
            double dx = ex - bx;
            double dy = ey - by;
            double dist = Math.sqrt(dx*dx + dy*dy);
            if((int)dist < e.getR() + b.getR()) {
               e.hit();
               bullets.remove(j);
               j--;
               boolean remove = e.remove();
               if(remove) {
                  enemies.remove(i);
                  i--;
                  break;
               }
               
            }
         }
         
      }
      
      
    //enemy's bullets - player collide
      for(int i = 0; i < bulletsEnemy.size(); i++) {
         EnemyBullet b = bulletsEnemy.get(i);
         double bx = b.getX();
         double by = b.getY();
         
         double dx = player.getX() - bx;
         double dy = player.getY() - by;
         double dist = Math.sqrt(dx*dx + dy*dy);
         if((int)dist < player.getR() + enemyBullet.getR()) {
            player.hit();
            if(player.getHealth() > 0) {
              countLife.remove(0);
            }         
            bulletsEnemy.remove(i);
            i--;
            
         }
      }
 
      //clash of player and enemies
      for(int i = 0; i < enemies.size(); i++) {
         Enemy e = enemies.get(i);
         double ex = e.getX();
         double ey = e.getY();
         double px = player.getX();
         double py = player.getY();
         double dx = ex - px;
         double dy = ey - py;
         double dist = Math.sqrt(dx * dx + dy * dy);
         if((int)dist <= e.getR() + player.getR()) {
            e.hit();
            player.hit();
            boolean remove = e.remove();
            if(remove) {
               enemies.remove(i);
               i--;
               
            }
         }
         
      }
      
      //wave update
      wave.update();
      
      // pause update
      pause.update();
      
      //life update
      life.update();
      
   }
   
   /**
    * drawing all the elements of the game
    */
   public void gameRender() {
      
      //background draw
      background.draw(g);
      
      //player draw
      player.draw(g);
      
      //bullets draw
      for(int i = 0; i < bullets.size(); i++) {
         bullets.get(i).draw(g);
      }
      
      //enemy's bullets draw
      for(int i = 0; i < bulletsEnemy.size(); i++) {
         bulletsEnemy.get(i).draw(g);
         
      }
      
      //enemies draw
      for(int i = 0; i < enemies.size(); i++) {
         enemies.get(i).draw(g);
         
      }
      
      //wave draw & wave end check
      if(wave.showWave()) {
         wave.draw(g);

         for(int i = 0; i < bulletsEnemy.size(); i++) {
            bulletsEnemy.remove(i);
         }
         for(int i = 0; i < countLife.size(); i++) {
            countLife.remove(i);
         }
         
      }
      
      // pause draw
      pause.draw(g);
      
      // life draw
      life.draw(g);
      
   }
   
   /**
    * canvas and brush creation
    */
   private void gameDraw() {
      Graphics g2 = this.getGraphics();
      g2.drawImage(image, 0, 0, null);
      g2.dispose();
   }
   
}

package ru.her.shooter;

import javax.swing.JFrame;

public class GameStart {
   
   public static void main(String [] args) {
      
      //game window creation
      GamePanel panel = new GamePanel();
      
      JFrame okno = new JFrame("Space War");
      okno.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
      okno.setResizable(false);
      okno.setContentPane(panel);
      okno.pack();
      okno.setLocationRelativeTo(null);
      okno.setVisible(true); 
      
      panel.start();
      
   }

}

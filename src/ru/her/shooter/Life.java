package ru.her.shooter;

import java.awt.*;

import javax.swing.ImageIcon;

public class Life {
   
   //Fields
   
   Image img;
   private int y;
   private int x;
   private int distanceBetwHearts;
   
   private int dist;
   private int r;

   //Construction
   
   public Life() {

      y = 15;
      x = 20;
      
      distanceBetwHearts = 5;
      
      r = 20;
      dist = 0;

   }
   
   //Functions
   
   public void update() {

   }
   
   public void draw(Graphics2D g) {
      
      for(int i = 0; i < GamePanel.countLife.size(); i++) {
         
         dist += distanceBetwHearts + r ;

         img = new ImageIcon("pictures\\heart.jpg").getImage();
         g.drawImage(img, x + dist, y, r, r, null);
      }
      
      dist = 0;
      
   }
     
}

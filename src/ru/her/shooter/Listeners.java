package ru.her.shooter;

import java.awt.event.*;

public class Listeners implements KeyListener, MouseListener, MouseMotionListener{
   
   @Override
   public void keyPressed(KeyEvent e) {
      
    int key = e.getKeyCode();
    
    if(key == KeyEvent.VK_W) {
       GamePanel.player.setUp(true);
    }
    if(key == KeyEvent.VK_S) {
       GamePanel.player.setDown(true);;
    }
    if(key == KeyEvent.VK_A) {
       GamePanel.player.setLeft(true);
    }
    if(key == KeyEvent.VK_D) {
       GamePanel.player.setRight(true);
    }
    if(key == KeyEvent.VK_SPACE) {
       GamePanel.player.setIsFiring(true);
    }
    if(key == KeyEvent.VK_ESCAPE) {
       GamePanel.state = GamePanel.STATES.MENUE;
       GamePanel.clickEsc = true;
    }
    
   }
   
   @Override
   public void keyReleased(KeyEvent e) {
      
      int key = e.getKeyCode();
      
      if(key == KeyEvent.VK_W) {
         GamePanel.player.setUp(false);
      }
      if(key == KeyEvent.VK_S) {
         GamePanel.player.setDown(false);
      }
      if(key == KeyEvent.VK_A) {
         GamePanel.player.setLeft(false);
      }
      if(key == KeyEvent.VK_D) {
         GamePanel.player.setRight(false);
      }
      if(key == KeyEvent.VK_SPACE) {
         GamePanel.player.setIsFiring(false);
      }
      if(key == KeyEvent.VK_ESCAPE) {
         GamePanel.clickEsc = false;
      }
   }
   
   @Override
   public void keyTyped(KeyEvent e) {
      // TODO Auto-generated method stub
   }

   @Override
   public void mouseDragged(MouseEvent e) {
      GamePanel.mouseX = e.getX();
      GamePanel.mouseY = e.getY();
      
   }

   @Override
   public void mouseMoved(MouseEvent e) {
      GamePanel.mouseX = e.getX();
      GamePanel.mouseY = e.getY();
      
   }

   @Override
   public void mouseClicked(MouseEvent arg0) {
      // TODO Auto-generated method stub
      
   }

   @Override
   public void mouseEntered(MouseEvent arg0) {
      // TODO Auto-generated method stub
      
   }

   @Override
   public void mouseExited(MouseEvent arg0) {
      // TODO Auto-generated method stub
      
   }

   @Override
   public void mousePressed(MouseEvent e) {
      if(e.getButton() == MouseEvent.BUTTON1) {//боттон 1 это левая клавиша
         GamePanel.player.setIsFiring(true);
         GamePanel.mouseClick = true;
      }
      
   }

   @Override
   public void mouseReleased(MouseEvent e) {
      if(e.getButton() == MouseEvent.BUTTON1) {
         GamePanel.player.setIsFiring(false);
         GamePanel.mouseClick = false;
      }
      
   }
   
}

package ru.her.shooter;

import java.awt.*;


public class Menue {
   
   //Fields
   private int windowWidth;
   private int windowHeight;
   
   private GamePanel gamePanel;

   private Color color1;
   private Color color2;

   private int transp1;
   private int transp2;

   //Construction
   
   public Menue() {
 
      windowWidth =  245;
      windowHeight = 60;
      
      gamePanel = new GamePanel();
      
      color1 = Color.white; 
      color2 = Color.white; 
      
      transp1 = 0;
      transp2 = 0;
      
   }
   
   //Functions
   
   public void update() {
      
      //updating the "Play!" button
      if(GamePanel.mouseX > gamePanel.WIDTH/2 - windowWidth/2 &&
            GamePanel.mouseX < gamePanel.WIDTH/2 + windowWidth/2 &&
            GamePanel.mouseY > gamePanel.HEIGHT/2 - (windowHeight + 5) &&
            GamePanel.mouseY < gamePanel.HEIGHT/2 - 5) {
         
         transp1 = 60;
         color1 = Color.GRAY; 
         
         if(GamePanel.mouseClick) {
            GamePanel.state = GamePanel.STATES.PLAY;
         }
       
      }else {
         transp1 = 0;
         color1 = Color.white; 
      }
      
      //updating the "INSTRUCTION" button
      if(GamePanel.mouseX > gamePanel.WIDTH/2 - windowWidth/2 &&
            GamePanel.mouseX < gamePanel.WIDTH/2 + windowWidth/2 &&
            GamePanel.mouseY > gamePanel.HEIGHT/2 + 5 &&
            GamePanel.mouseY < gamePanel.HEIGHT/2 + windowHeight + 5) {
         
         transp2 = 60;
         color2 = Color.GRAY; 
         
         if(GamePanel.mouseClick) {
            GamePanel.state = GamePanel.STATES.INSTR;
         }
         
      }else {
         transp2 = 0;
         color2 = Color.white; 
      }
        
   }
   
   public void draw(Graphics2D g) {

      //draw window "Play!"
      g.setColor(color1);
      g.setStroke(new BasicStroke(3));
      
      g.drawRect(gamePanel.WIDTH/2 - windowWidth/2, 
            gamePanel.HEIGHT/2 - (windowHeight + 5), 
            windowWidth, windowHeight);
      
      g.setColor(new Color(255, 255, 255, transp1));
      g.fillRect(gamePanel.WIDTH/2 - windowWidth/2, 
            gamePanel.HEIGHT/2 - (windowHeight + 5), 
            windowWidth, windowHeight);
      
      g.setStroke(new BasicStroke(1)); 
      
      g.setColor(color1);
      g.setFont(new Font("Consolas", Font.BOLD, 40));
      long  length = (int)g.getFontMetrics().getStringBounds("Play!", g).getWidth();
      g.drawString("Play!", (int)(gamePanel.WIDTH/2 - length/2),
            (int)(gamePanel.HEIGHT/2 - windowHeight/2 - 5 + windowHeight/4));
      
      
      
      //draw window "Instruction"
      g.setColor(color2);
      g.setStroke(new BasicStroke(3));
      
      g.drawRect(gamePanel.WIDTH/2 - windowWidth/2, 
            gamePanel.HEIGHT/2 + 5, 
            windowWidth, windowHeight);
      
      g.setColor(new Color(255, 255, 255, transp2));
      g.fillRect(gamePanel.WIDTH/2 - windowWidth/2, 
            gamePanel.HEIGHT/2 + 5, 
            windowWidth, windowHeight);
      
      g.setStroke(new BasicStroke(1)); 
      
      g.setColor(color2);
      g.setFont(new Font("Consolas", Font.BOLD, 40));
      long  length2 = (int)g.getFontMetrics().getStringBounds("Instruction", g).getWidth();
      g.drawString("Instruction", (int)(gamePanel.WIDTH/2 - length2/2),
            (int)(gamePanel.HEIGHT/2 + windowHeight/2 + windowHeight/4));
      
   }
     
}

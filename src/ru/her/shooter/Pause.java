package ru.her.shooter;

import java.awt.*;

import ru.her.shooter.GamePanel.STATES;


public class Pause {
   
   //Fields
   private int r;
   
   private Color color1;
   private Color color2;
   
   private int transp1;
   private int transp2;

   private int windowWidth;
   private int windowHeight;
   
   private GamePanel gamePanel;

   //Construction
   public Pause() {
      
      r = 20;
      
      color1 = Color.white;  
      color2 = Color.white;  
      
      transp1 = 0;
      transp2 = 0;
      
      windowWidth = 240;
      windowHeight = 60;
      
      gamePanel = new GamePanel();
   }
   
   //Functions
   
   public void update() {
      
      //updating the pause button
      if(GamePanel.mouseX > 15 &&
            GamePanel.mouseX < 15 + r &&
            GamePanel.mouseY > 15 &&
            GamePanel.mouseY < 15 + r) {
         
         transp1 = 60;
         color1 = Color.GRAY;  
         
         if(GamePanel.mouseClick) {
            GamePanel.state = GamePanel.STATES.PAUSE;
         }
     
      }else {
         
         transp1 = 0;
         color1 = Color.white;  
      }
      
      //updating the "Continue!" button
      if(GamePanel.state.equals(STATES.PAUSE) &&
            GamePanel.mouseX > gamePanel.WIDTH/2 - windowWidth/2 &&
            GamePanel.mouseX < gamePanel.WIDTH/2 + windowWidth/2 &&
            GamePanel.mouseY > gamePanel.HEIGHT/2 - windowHeight/2 &&
            GamePanel.mouseY < gamePanel.HEIGHT/2 + windowHeight/2) {
         

         transp2 = 60;
         color2 = Color.GRAY;  
         
         if(GamePanel.mouseClick) {
            GamePanel.state = GamePanel.STATES.PLAY;
         }
      
         
      }else {
         
         transp2 = 0;
         color2 = Color.white;  
      }
        
   }
   
   public void draw(Graphics2D g) {

      //draw pause window
      if(GamePanel.state.equals(STATES.PLAY)) {
         g.setColor(color1);
         g.setStroke(new BasicStroke(3));
         
         g.drawRect(15, 15, r, r);
         
         g.setColor(new Color(255, 255, 255, transp1));
         g.fillRect(15, 15, r, r);
         
         g.setStroke(new BasicStroke(1)); 
         
         g.setColor(color1);
         g.setFont(new Font("Consolas", Font.BOLD, 12));
         long  length = (int)g.getFontMetrics().getStringBounds("||", g).getWidth();
         g.drawString("||", (int)(15 + r/2 - length/2),
               (int)(r/2 + 15 + r/4));
      }
      
      //draw window "Continue!"
      if(GamePanel.state.equals(STATES.PAUSE)) {
         g.setColor(color2);
         g.setStroke(new BasicStroke(3));
         
         g.drawRect(gamePanel.WIDTH/2 - windowWidth/2, gamePanel.HEIGHT/2 - windowHeight/2,
               windowWidth, windowHeight);
         
         g.setColor(new Color(255, 255, 255, transp2));
         g.fillRect(gamePanel.WIDTH/2 - windowWidth/2, gamePanel.HEIGHT/2 - windowHeight/2,
               windowWidth, windowHeight);
         
         g.setStroke(new BasicStroke(1)); 
         
         g.setColor(color2);
         g.setFont(new Font("Consolas", Font.BOLD, 40));
         long  length = (int)g.getFontMetrics().getStringBounds("Сontinue!", g).getWidth();
         g.drawString("Сontinue!", (int)(gamePanel.WIDTH/2 - length/2),
               (int)(gamePanel.HEIGHT/2 + windowHeight/4));
      }
      
      
   }
     
}
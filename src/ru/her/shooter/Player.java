package ru.her.shooter;

import java.awt.*;

import javax.swing.ImageIcon;

public class Player {
   
   //Fields
   
   private GamePanel gamePanel;
   
   private double x;
   private double y;
   private int r;
   
   Image img;
      
   private double dx;
   private double dy;
   
   private int speed;
   
   private int health;
   
   private long time;
   private long timer;
   private long  controlTimer;

   private boolean up;
   private boolean down;
   private boolean left;
   private boolean right;
   private boolean isFiring;
   
   //constructor 
   
   public Player() {
      
      gamePanel = new GamePanel();
      
      x = gamePanel.WIDTH/2;
      y = gamePanel.HEIGHT/2; 
      
      r = 50;
      
      speed = 5;
      
      dx = 0;
      dy = 0;
      
      health = 10;
 
      time = System.nanoTime();
      
      up = false;
      down = false;
      left = false;
      right = false;
      isFiring = false;

   }
   
   //Functions
   
   // get x
   public double getX() {return x;}
   
   // get y
   public double getY() {return y;}
   
   // get r
   public double getR() {return r;}
   
   // get health
   public int getHealth() {return health;}
   
   // update lives after each level
   public void setHealth(int health) {this.health = health;}
   
   // damage after a bullet hit
   public void hit() {health--;}
   
   // set Up
   public void setUp(boolean up) {this.up = up;}
   
   // set Down
   public void setDown(boolean down) {this.down = down;}
   
   // set Left
   public void setLeft(boolean left) {this.left = left;}
   
   // set Right
   public void setRight(boolean right) {this.right = right;}
   
   // set isFiring
   public void setIsFiring(boolean isFiring) {this.isFiring = isFiring;}
   
   // get Up
   public boolean getUp() {return up;}
   
   // get Down
   public boolean getDown() {return down;}
   
   // get Left
   public boolean getLeft() {return left;}
   
   // get Right
   public boolean getRight() {return right;}
   
   // get isFiring
   public boolean getIsFiring() {return isFiring;}
   
   // bullet generation frequency
   public boolean generationBullet() {

      timer = (System.nanoTime() - time) / 100000000;

      if(isFiring && timer%3==0 && controlTimer != timer) {
         
         controlTimer = timer;
         return true;
      }
      return false;
   }

   public void update() {
      
      //player control
      if(up && y > gamePanel.HEIGHT/2) dy = -speed;
      
      if(down && y < gamePanel.HEIGHT - r) dy = speed;
      
      if(left && x > r) dx = -speed;
      
      if(right && x < gamePanel.WIDTH - r) dx = speed;
      
      if(up && left || up && right || down && left || down && right) {
         double angle = Math.toRadians(45);
         dy = dy* Math.sin(angle);
         dx = dx * Math.cos(angle);
      }
      
      y += dy;
      x += dx;
      
      dx = 0;
      dy = 0;

   }
   
   public void draw(Graphics2D g) {
      
      // draw player
      img = new ImageIcon("pictures\\player.jpg").getImage();
      g.drawImage(img, (int) (x - r), (int) (y - r), 2*r, 2*r, null);
      
   }

}

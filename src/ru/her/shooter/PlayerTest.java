package ru.her.shooter;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class PlayerTest {
   
   Player player = new Player();
   GamePanel gamePanel = new GamePanel();
   
   @Test
   void testPositionX() {
      double x = player.getX();
      
      assertEquals(gamePanel.WIDTH/2, x);
      
   }
   
   @Test
   void testPositionY() {
      double y = player.getY();
      
      assertEquals(gamePanel.HEIGHT/2, y);
      
   }
   
   @Test
   void testPositionR() {
      double r = player.getR();
      
      assertEquals(50, r);
      
   }
   
   @Test
   void testHealthPlayer() {
      
      player.hit();
      int health = player.getHealth();

      assertEquals(9, health);
      
   }
   
   @Test
   void testSetUp() {
      
      player.setUp(true);
      boolean up = player.getUp();

      assertEquals(true, up);
      
   }
   
   @Test
   void testSetDown() {
      
      player.setDown(true);
      boolean down = player.getDown();

      assertEquals(true, down);
      
   }
   
   @Test
   void testSetLeft() {
      
      player.setLeft(true);
      boolean left = player.getLeft();

      assertEquals(true, left);
   
   }
   
   @Test
   void testSetRight() {
      
      player.setRight(true);
      boolean right = player.getRight();

      assertEquals(true, right);
   
   }
   
   @Test
   void testSetIsFiring() {
      
      player.setIsFiring(true);
      boolean isFiring = player.getIsFiring();

      assertEquals(true, isFiring);
   
   }

}

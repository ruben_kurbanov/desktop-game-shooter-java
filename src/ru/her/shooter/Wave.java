package ru.her.shooter;

import java.awt.*;

public class Wave {

   //Fields
   
   private int waveNumber;
   
   private long waveTimer;
   private long waveDelay;
   private long waveTimerDiff;
 
   private int enemyCount;
   private int enemy2Count;

   private String waveText;
   
   private GamePanel gamePanel;
   
   //Constructions
   
   public Wave() {
      
      waveNumber = 0;
      enemyCount = 0;
      enemy2Count = 0;
      
      waveTimer = 0;
      waveDelay = 5000; // 5 second
      waveTimerDiff = 0;

      waveText = "L E V E L - ";
      
      gamePanel = new GamePanel();
   }
   
   //Functions
   
   public int showEnemyCount(){return enemyCount + enemy2Count;}
   
   public int showWaveCount() {return waveNumber;}
   
   //level start check
   public boolean showWave() {
      
      if(waveTimer != 0) {
         return true;
      }else return false;
        
   }
   
   public void createEnemies() {

      GamePanel.player.setHealth(10);
      
      for(int i = 0; i < 10; i++) {
         GamePanel.countLife.add(new Life());
      }
      
      //script levels
      switch(waveNumber) {
           
         case(0):
               enemyCount = 1;
               while(enemyCount > 0) {
                  GamePanel.enemies.add(new Enemy(1));
                  enemyCount--;
               }

            break;
      
         case(1):
               enemyCount = 5;
               while(enemyCount > 0) {
                  GamePanel.enemies.add(new Enemy(1));
                  enemyCount--;
               }

            break;
            
         case(2):
               enemyCount = 10;
               while(enemyCount > 0) {
                     GamePanel.enemies.add(new Enemy(1));
                     enemyCount--;
               }
            break;
            
         case(3):
               enemyCount = 15;
               while(enemyCount > 0) {
                  GamePanel.enemies.add(new Enemy(1));
                  enemyCount--;
               }
            break;
            
         case(4):
               enemyCount = 20;
               while(enemyCount > 0) {
                  GamePanel.enemies.add(new Enemy(1));
                  enemyCount--;
               }
            
            break;
         
         case(5):
               enemy2Count = 3;
               while(enemy2Count > 0) {
                  GamePanel.enemies.add(new Enemy(2));
                  enemy2Count--;
               }
         
            break;
         
         case(6):
               enemy2Count = 5;
               while(enemy2Count > 0) {
                  GamePanel.enemies.add(new Enemy(2));
                  enemy2Count--;
               }
         
            break;
         
         case(7):
               enemyCount = 3;
               enemy2Count = 5;
               while(enemyCount > 0) {
                  GamePanel.enemies.add(new Enemy(1));
                  enemyCount--;
               }
               
               while(enemy2Count > 0) {
                  GamePanel.enemies.add(new Enemy(2));
                  enemy2Count--;
               }
         
            break;
         
         case(8):
               enemyCount = 5;
               enemy2Count = 7;
               while(enemyCount > 0) {
                  GamePanel.enemies.add(new Enemy(1));
                  enemyCount--;
               }
               while(enemy2Count > 0) {
                  GamePanel.enemies.add(new Enemy(2));
                  enemy2Count--;
               }
         
            break;
         
         case(9):
            
            enemyCount = 7;
            enemy2Count = 7;
            while(enemyCount > 0) {
               GamePanel.enemies.add(new Enemy(1));
               enemyCount--;
            }
            while(enemy2Count > 0) {
               GamePanel.enemies.add(new Enemy(2));
               enemy2Count--;
            }
         
            break;
            
         case(10):
               enemyCount = 1;
               GamePanel.enemies.add(new Enemy(3));
               enemyCount--;
            break;
   
      }
            
       waveNumber ++;
       
   }
 
   public void update() {
      
      //transition to another level
      if(GamePanel.enemies.size() == 0 && waveTimer == 0) {
         waveTimer = System.nanoTime();
      }
      
      if(waveTimer > 0) {
         waveTimerDiff += (System.nanoTime() - waveTimer)/1000000;
         waveTimer = System.nanoTime();
      }
      
      if(waveTimerDiff > waveDelay) {
         createEnemies();
         waveTimer = 0;
         waveTimerDiff = 0;
      }
   }
   
   public void draw(Graphics2D g) {
      
      double divider = waveDelay/180;
      double alpha = waveTimerDiff/divider;
      alpha = 255 * Math.sin(Math.toRadians(alpha));
      
      if(alpha < 0) alpha = 0;
      if(alpha > 255) alpha = 255;
      
      g.setFont(new Font ("consolas", Font.ITALIC, 40));
      g.setColor(new Color(255, 255, 255, (int)alpha));
      String s = waveText + waveNumber;
      
      long length = (int)g.getFontMetrics().getStringBounds(s, g).getWidth();
      
      g.drawString(s, gamePanel.WIDTH/2 - (int)length/2, gamePanel.HEIGHT/2);
   }

}

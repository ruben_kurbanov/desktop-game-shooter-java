package ru.her.shooter;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class WaveTest {

   Wave wave = new Wave();
   
   @Test
   void testShowWave() {
      boolean show = wave.showWave();
      assertEquals(false, show);
   }
   
   @Test
   void testShowEnemyCount() {
      
      int show = wave.showEnemyCount();
      assertEquals(0, show);
   }
   
   @Test
   void testShowWaveCount() {

      int show = wave.showWaveCount();
      assertEquals(0, show);
   }

}
